package com.sbs.designPattern;

import java.util.Iterator;

public class IteratorPattern {

	public static void main(String[] args) {
		MyArray<Integer> array = new MyArray <Integer> ();
		Iterator<?> itr = array.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
		System.out.println("##### Using for each loop ########");
		for (Object i: array) {
			System.out.println(i);
		}
	}

}

class MyArray <T> implements Iterable{
   
	private int count ;
	private Object [] array = new Object [] {12,13,45,32,34,11};
	
	public MyArray() {
    }
	
	public MyArray(T [] array) {
	   this.array = array;	
	}
	
	 class MyIterator implements  Iterator<T> {

		public boolean hasNext() {
			if(count<array.length)
			return true;
			else
			return false;
		}

		public T next() {
			return (T) array[count++];
		}	
	}	 

	public Iterator <T> iterator() {
		  return new MyIterator();
	}	 
}



