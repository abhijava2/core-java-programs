package com.sbs.datastructure;

public class ArrayPractice {

	public static void main(String[] args) {
		// Programmed to get second highest of array.		
		int array [] = {46,48,20,30,6,45,98};
		int secondHigh = getSecondHighest(array);
         System.out.println(secondHigh);
	}

	public static int getSecondHighest(int array[]) {
		int first = array[0];
		int second = array[0];
		for (int i = 1; i < array.length; i++) {
			if (array[i] > first) {
				second = first;
				first = array[i];
			} else if (array[i] > second) {
				second = array[i];
			}
		}
		return second;
	}
}
