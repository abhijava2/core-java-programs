package com.sbs.utils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;

public class Java8DateApi {

  public static void main(String[] args) {

    System.out.println("-----------------------");
    Instant instant1 = Instant.now();
    try {
      Thread.sleep(2000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    Instant instant2 = Instant.now();
    Duration duration = Duration.between(instant1, instant2);
    System.out.println(duration.getSeconds());

    Optional<Integer> possible = Optional.ofNullable(null);
    System.out.println(possible.isPresent());

    Optional<String> possible1 = Optional.empty();
    System.out.println(possible1.isPresent());
    // System.out.println(possible1.get()); // will throw exception no such element.
    // Calculator calculator = possible1.orElse(new Calculator());
    String str = possible1.orElse(new String("hello"));
    System.out.println(str);
    // NPE Optional<String> possible2 = Optional.of(null);
    Optional<String> possible3 = Optional.ofNullable("value passed");
    System.out.println(possible3.get());

    // Get the file reference
    Path path = Paths.get("C:\\dino-files\\output.txt");

    // Use try-with-resource to get auto-closeable writer instance
    try (BufferedWriter writer = Files.newBufferedWriter(path)) {
      writer.write("Hello World !!");
    } catch (IOException e) {
      e.printStackTrace();
    }

    String content = "Hello Java 8 !!";

    try {
      Files.write(Paths.get("C:\\dino-files\\output.txt"), content.getBytes());
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

}
