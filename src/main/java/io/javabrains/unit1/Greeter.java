package io.javabrains.unit1;

public class Greeter {

	public void greet(Greeting greeting) {
		greeting.perform();
	}
	
	
	public static void main(String[] args) {
		Greeter greeter = new Greeter();				
		Greeting lambdaGreeting = () -> System.out.println("Hello world Lambda!");
		lambdaGreeting.perform();
		Greeting innerClassGreeting = new Greeting() {
			public void perform() {
				System.out.print("Hello world!");
			}
		};		
		greeter.greet(() -> System.out.println("Hello world using functional interface passing!"));
		greeter.greet(innerClassGreeting);	
		greeter.greet(lambdaGreeting);				
		HelloWorldGreeting h = new HelloWorldGreeting();
		h.perform();
		greeter.greet(h);
		Greeting test =  h :: perform;
		greeter.greet(test);

	}

}



